﻿##########
# France #
##########

# Valois-Bourgogne
house_bourgogne_nevers = {
	name = "dynn_bourgogne_nevers"
	dynasty = CKU_valois_bourgogne
}

# Capet-Bourgogne
house_bourgogne_montagu = {
	name = "dynn_bourgogne_montagu"
	dynasty = 741
}

house_bourgogne_aviz = {
	name = "dynn_bourgogne_aviz"
	dynasty = 741
}

# Luxembourg
house_luxembourg_bohemia = {
	prefix = "dynnp_de"
	name = "dynn_luxembourg_bohemia"
	dynasty = 420
}

house_luxembourg_ligny = {
	prefix = "dynnp_de"
	name = "dynn_luxembourg_ligny"
	dynasty = 420
}

# Valois
house_alencon = {
	prefix = "dynnp_d_"
	name = "dynn_alencon"
	dynasty = 101
}

house_anjou = {
	prefix = "dynnp_d_"
	name = "dynn_anjou"
	dynasty = 101
}

house_orleans = {
	prefix = "dynnp_d_"
	name = "dynn_orleans"
	dynasty = 101
}

house_angouleme = {
	prefix = "dynnp_d_"
	name = "dynn_angouleme"
	dynasty = 101
}

house_longueville = { # Cadet branch of Valois-Orleans
	prefix = "dynnp_de"
	name = "dynn_longueville"
	dynasty = 101
}

# Capetian Anjou
house_anjou_durazzo = {
	prefix = "dynnp_d_"
	name = "dynn_anjou_durazzo"
	dynasty = 81
}

# Foix
house_foix_grailly = {
	prefix = "dynnp_de"
	name = "dynn_foix_grailly"
	dynasty = CKU_foix
}
house_foix_castelbon = {
	prefix = "dynnp_de"
	name = "dynn_foix_castelbon"
	dynasty = CKU_foix
}

# Laval
house_montmorency_laval = {
	prefix = "dynnp_de"
	name = "dynn_montmorency_laval"
	dynasty = CKU_laval
}

house_montfort_laval = {
	prefix = "dynnp_de"
	name = "dynn_montfort_laval"
	dynasty = CKU_laval
}

# Dreux
house_dreux_montfort = {
	name = "dynn_dreux_montfort"
	dynasty = 86
}
house_dreux_beu = {
	name = "dynn_dreux_beu"
	dynasty = 86
}
house_dreux_machecoul = {
	name = "dynn_dreux_machecoul"
	dynasty = 86
}

# Baux
house_baux_orange = {
    prefix = "dynnp_des"
    name = "dynn_baux_orange"
    dynasty = 5117
}
house_baux_berre = {
    prefix = "dynnp_des"
    name = "dynn_baux_berre"
    dynasty = 5117
}
house_baux_andria = {
    prefix = "dynnp_des"
    name = "dynn_baux_andria"
    dynasty = 5117
}
house_baux_marignan = {
    prefix = "dynnp_des"
    name = "dynn_baux_marignan"
    dynasty = 5117
}
house_baux_meyrargues = {
    prefix = "dynnp_des"
    name = "dynn_baux_meyrargues"
    dynasty = 5117
}
house_baux_courthezon = {
    prefix = "dynnp_des"
    name = "dynn_baux_courthezon"
    dynasty = 5117
}

# Chabannes
house_chabannes_dammartin = {
	prefix = "dynnp_de"
	name = "dynn_chabannes_dammartin"
	dynasty = CKU_chabannes
}

# Limoges
house_limoges_comborn = {
	prefix = "dynnp_de"
	name = "dynn_limoges_comborn"
	dynasty = CKU_limoges
}
house_limoges_turenne = {
	prefix = "dynnp_de"
	name = "dynn_limoges_turenne"
	dynasty = CKU_limoges
}
house_limoges_ventadour = {
	prefix = "dynnp_de"
	name = "dynn_limoges_ventadour"
	dynasty = CKU_limoges
}
house_limoges_aubusson = {
	prefix = "dynnp_de"
	name = "dynn_limoges_aubusson"
	dynasty = CKU_limoges
}
house_limoges_brosse = {
	prefix = "dynnp_de"
	name = "dynn_limoges_brosse"
	dynasty = CKU_limoges
}
house_limoges_rochechouart = {
	prefix = "dynnp_de"
	name = "dynn_limoges_rochechouart"
	dynasty = CKU_limoges
}

# Dinan
house_dinan_dinham = {
	name = "dynn_dinan_dinham"
	dynasty = CKU_dinan
}
house_dinan_belliere = {
	name = "dynn_dinan_belliere"
	dynasty = CKU_dinan
}

# Motier de La Fayette
house_motier_champetieres = {
	name = "dynn_motier_champetieres"
	dynasty = CKU_motier_dela_fayette
}

# Porhoet
house_rohan = {
	prefix = "dynnp_de"
	name = "dynn_rohan"
	dynasty = CKU_porhoet
}
house_rohan_guemene = {
	prefix = "dynnp_de"
	name = "dynn_rohan_guemene"
	dynasty = CKU_porhoet
}
house_rohan_gue_de_lisle = {
	prefix = "dynnp_de"
	name = "dynn_rohan_gue_de_lisle"
	dynasty = CKU_porhoet
}
house_rohan_montauban = {
	prefix = "dynnp_de"
	name = "dynn_rohan_montauban"
	dynasty = CKU_porhoet
}
house_rohan_bois_de_la_roche = {
	prefix = "dynnp_de"
	name = "dynn_rohan_bois_de_la_roche"
	dynasty = CKU_porhoet
}
house_zouche = {
	name = "dynn_zouche"
	dynasty = CKU_porhoet
}

# Melun
house_melun_laloupe = {
	prefix = "dynnp_de"
	name = "dynn_melun_laloupe"
	dynasty = CKU_melun
}
house_melun_laborde = {
	prefix = "dynnp_de"
	name = "dynn_melun_laborde"
	dynasty = CKU_melun
}
house_melun_antoing = {
	prefix = "dynnp_de"
	name = "dynn_melun_antoing"
	dynasty = CKU_melun
}

# Estouteville
house_estouteville_grousset = {
	prefix = "dynnp_d_"
	name = "dynn_estouteville_grousset"
	dynasty = CKU_estouteville
}
house_estouteville_criquebeuf = {
	prefix = "dynnp_d_"
	name = "dynn_estouteville_criquebeuf"
	dynasty = CKU_estouteville
}
house_estouteville_torcy = {
	prefix = "dynnp_d_"
	name = "dynn_estouteville_torcy"
	dynasty = CKU_estouteville
}
house_estouteville_rames = {
	prefix = "dynnp_d_"
	name = "dynn_estouteville_rames"
	dynasty = CKU_estouteville
}
house_estouteville_bouchet = {
	prefix = "dynnp_d_"
	name = "dynn_estouteville_bouchet"
	dynasty = CKU_estouteville
}
house_estouteville_villebon = {
	prefix = "dynnp_d_"
	name = "dynn_estouteville_villebon"
	dynasty = CKU_estouteville
}

# Joux
house_joux_usie = {
	prefix = "dynnp_d_"
	name = "dynn_joux_usie"
	dynasty = CKU_joux
}
house_joux_naisey = {
	prefix = "dynnp_de"
	name = "dynn_joux_naisey"
	dynasty = CKU_joux
}

# Astarac
house_astarac_montlezun = {
	prefix = "dynnp_de"
	name = "dynn_astarac_montlezun"
	dynasty = CKU_astarac
}

# Fezensac
house_fezensac_armagnac = {
	prefix = "dynnp_d_"
	name = "dynn_fezensac_armagnac"
	dynasty = CKU_fezensac
}
house_fezensac_montesquiou = {
	prefix = "dynnp_de"
	name = "dynn_fezensac_montesquiou"
	dynasty = CKU_fezensac
}
house_fezensac_montluc = {
	prefix = "dynnp_de"
	name = "dynn_fezensac_montluc"
	dynasty = CKU_fezensac
}
house_fezensac_montesquiou_marsan = {
	prefix = "dynnp_de"
	name = "dynn_fezensac_montesquiou_marsan"
	dynasty = CKU_fezensac
}

# Lomagne
house_lomagne_batx = {
	prefix = "dynnp_de"
	name = "dynn_fezensac_batx"
	dynasty = CKU_lomagne
}
house_lomagne_armagnac = {
	prefix = "dynnp_de"
	name = "dynn_fezensac_armagnac"
	dynasty = CKU_lomagne
}
house_lomagne_gimat = {
	prefix = "dynnp_de"
	name = "dynn_fezensac_gimat"
	dynasty = CKU_lomagne
}
house_lomagne_fimarcon = {
	prefix = "dynnp_de"
	name = "dynn_fezensac_fimarcon"
	dynasty = CKU_lomagne
}
house_lomagne_armagnac_termes = {
	prefix = "dynnp_d_"
	name = "dynn_fezensac_armagnac_termes"
	dynasty = CKU_lomagne
}
house_lomagne_armagnac_fezensaguet = {
	prefix = "dynnp_d_"
	name = "dynn_fezensac_armagnac_fezensaguet"
	dynasty = CKU_lomagne
}

# Levis
house_levis_florensac = {
	prefix = "dynnp_de"
	name = "dynn_levis_florensac"
	dynasty = CKU_levis
}
house_levis_lautrec = {
	prefix = "dynnp_de"
	name = "dynn_levis_lautrec"
	dynasty = CKU_levis
}

# Bourbon
house_bourbon_la_marche = {
	prefix = "dynnp_de"
	name = "dynn_bourbon_la_marche"
	dynasty = CKU_bourbon
}
house_bourbon_preaux = {
	prefix = "dynnp_de"
	name = "dynn_bourbon_preaux"
	dynasty = CKU_bourbon
}
house_bourbon_vendome = {
	prefix = "dynnp_de"
	name = "dynn_bourbon_vendome"
	dynasty = CKU_bourbon
}
house_bourbon_carency = {
	prefix = "dynnp_de"
	name = "dynn_bourbon_carency"
	dynasty = CKU_bourbon
}
house_bourbon_montpensier = {
	prefix = "dynnp_de"
	name = "dynn_bourbon_montpensier"
	dynasty = CKU_bourbon
}

# Beauvau
house_beauvau_craon = {
	prefix = "dynnp_de"
	name = "dynn_beauvau_craon"
	dynasty = CKU_beauvau
}

# Ivrea
house_ivrea_burgundy_spain = {
	prefix = "dynnp_de"
	name = "dynn_ivrea_burgundy_spain"
	dynasty = CKU_ivrea
}
house_ivrea_chalon = {
	prefix = "dynnp_de"
	name = "dynn_ivrea_chalon"
	dynasty = CKU_ivrea
}
house_ivrea_salins = {
	prefix = "dynnp_de"
	name = "dynn_ivrea_salins"
	dynasty = CKU_ivrea
}
house_ivrea_chalon_auxerre = {
	prefix = "dynnp_de"
	name = "dynn_ivrea_chalon_auxerre"
	dynasty = CKU_ivrea
}
house_ivrea_chalon_arlay = {
	prefix = "dynnp_de"
	name = "dynn_ivrea_chalon_arlay"
	dynasty = CKU_ivrea
}
house_ivrea_chalon_montbeliard = {
	prefix = "dynnp_de"
	name = "dynn_ivrea_chalon_montbeliard"
	dynasty = CKU_ivrea
}

# Caumont
house_caumont_lauzun = {
	prefix = "dynnp_de"
	name = "dynn_caumont_lauzun"
	dynasty = CKU_caumont
}

# Comminges
house_comminges_couserans = {
	name = "dynn_comminges_couserans"
	dynasty = CKU_comminges
}

# Montoire
house_montoire_vendome = {
	name = "dynn_montoire_vendome"
	dynasty = CKU_montoire
}

# Agoult
house_agoult_simiane = {
	name = "dynn_agoult_simiane"
	dynasty = CKU_agoult
}

# Courtenay
house_courtenay_champignelles = {
	name = "dynn_courtenay_champignelles"
	dynasty = CKU_courtenay
}
house_courtenay_la_ferte_loupiere = {
	name = "dynn_courtenay_la_ferte_loupiere"
	dynasty = CKU_courtenay
}
house_courtenay_autry = {
	name = "dynn_courtenay_autry"
	dynasty = CKU_courtenay
}
house_courtenay_bleneau = {
	name = "dynn_courtenay_bleneau"
	dynasty = CKU_courtenay
}
house_courtenay_tanlay = {
	name = "dynn_courtenay_tanlay"
	dynasty = CKU_courtenay
}

# Lusignan
house_lusignan_jerusalem = {
	prefix = "dynnp_de"
	name = "dynn_lusignan_jerusalem"
	dynasty = 93
}

#################
# Great Britain #
#################


###############
# Netherlands #
###############
# Holland (Netherlands)
house_brederode = {
	prefix = "dynnp_van"
	name = "dynn_brederode"
	culture = "dutch"
	dynasty = CKU_holland
}
house_vander_duyn = {
	prefix = "dynnp_van_der"
	name = "dynn_vander_duyn"
	culture = "dutch"
	dynasty = CKU_holland
}
house_adrichem = {
	prefix = "dynnp_van"
	name = "dynn_adrichem"
	culture = "dutch"
	dynasty = CKU_holland
}

# Wassenaer
house_duivenvoorde = {
	prefix = "dynnp_van"
	name = "dynn_duivenvoorde"
	culture = "dutch"
	dynasty = CKU_wassenaer
}
house_santhorst = {
	prefix = "dynnp_van"
	name = "dynn_santhorst"
	culture = "dutch"
	dynasty = CKU_wassenaer
}
house_cranenburch = {
	prefix = "dynnp_van"
	name = "dynn_cranenburch"
	culture = "dutch"
	dynasty = CKU_wassenaer
}
house_polanen = {
	prefix = "dynnp_van"
	name = "dynn_polanen"
	culture = "dutch"
	dynasty = CKU_wassenaer
}
house_vander_leck = {
	prefix = "dynnp_van_der"
	name = "dynn_leck"
	culture = "dutch"
	dynasty = CKU_wassenaer
}
house_vanden_bergh = {
	prefix = "dynnp_van_den"
	name = "dynn_vanden_bergh"
	culture = "dutch"
	dynasty = CKU_wassenaer
}

# Horne
house_horne_houtekerke = {
	prefix = "dynnp_van"
	name = "dynn_horne_houtekerke"
	culture = "dutch"
	dynasty = CKU_horne
}
house_cranendonck = {
	name = "dynn_horne_cranendonck"
	culture = "dutch"
	dynasty = CKU_horne
}

# Brabant
house_withem = {
	prefix = "dynnp_van"
	name = "dynn_withem"
	culture = "dutch"
	dynasty = CKU_brabant
}
house_berghes = {
	prefix = "dynnp_van"
	name = "dynn_berghes"
	culture = "dutch"
	dynasty = CKU_brabant
}